#ifndef GAMEMANAGER_H   // if x.h hasn't been included yet...
#define GAMEMANAGER_H

//Using SDL and standard IO
#include <SDL.h>
#include <SDL_opengl.h>
#include <GL\GLU.h>
#include <stdio.h>

class GameManager 
{
public:
	GameManager();
	~GameManager();

	void init();
	bool initGL();
	void draw();
private:
	 //The window we'll be rendering to
    SDL_Window* window;    
    //The surface contained by the window
    SDL_GLContext screenSurface;

	//Screen dimension constants
	int SCREEN_WIDTH;
	int SCREEN_HEIGHT;
};

#endif