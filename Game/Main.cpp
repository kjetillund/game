#include "GameManager.h"

int main( int argc, char* args[] )
{
   
	GameManager *gameManager = new GameManager();

	gameManager->init();

	gameManager->draw();   

	delete gameManager;

    return 0;
}
