#include "GameManager.h"

GameManager::GameManager()
{
	 //The window we'll be rendeNUring to
	window = NULL;    
    //The surface contained by the window
    screenSurface = NULL;
	
	SCREEN_WIDTH = 640;
	SCREEN_HEIGHT = 480;
}
GameManager::~GameManager()
{
	 //Destroy window
    SDL_DestroyWindow( window );

    //Quit SDL subsystems
    SDL_Quit();
}
void GameManager::init()
{
	 //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    }
	else
    {
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 2 );
		SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );

        //Create window
        window = SDL_CreateWindow( "SDL window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN  );
        if( window == NULL )
        {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
        }
		 else
        {
            //Get window surface
            screenSurface = SDL_GL_CreateContext( window ); 

			//Use Vsync
			if( SDL_GL_SetSwapInterval( 1 ) < 0 )
			{
				printf( "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError() );
			}

			//Initialize OpenGL
			if( !initGL() )
			{
				printf( "Unable to initialize OpenGL!\n" );
			}
        }
    }	
}
bool GameManager::initGL()
{
	bool success = true;
	GLenum error = GL_NO_ERROR;

	//Initialize Projection Matrix
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	
	//Check for error
	error = glGetError();
	if( error != GL_NO_ERROR )
	{
		printf( "Error initializing OpenGL! %s\n", gluErrorString( error ) );
		success = false;
	}

	//Initialize Modelview Matrix
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	//Check for error
	error = glGetError();
	if( error != GL_NO_ERROR )
	{
		printf( "Error initializing OpenGL! %s\n", gluErrorString( error ) );
		success = false;
	}
	
	//Initialize clear color
	glClearColor( 0.f, 0.f, 0.f, 1.f );
	
	//Check for error
	error = glGetError();
	if( error != GL_NO_ERROR )
	{
		printf( "Error initializing OpenGL! %s\n", gluErrorString( error ) );
		success = false;
	}
	
	return success;
}
void GameManager::draw()
{
	//Clear color buffer
	glClear( GL_COLOR_BUFFER_BIT );
	
	//Render quad	
	glBegin( GL_QUADS );
		glVertex2f( -0.5f, -0.5f );
		glVertex2f( 0.5f, -0.5f );
		glVertex2f( 0.5f, 0.5f );
		glVertex2f( -0.5f, 0.5f );
	glEnd();	

	SDL_GL_SwapWindow( window );

    //Wait two seconds
    SDL_Delay( 2000 );
}
